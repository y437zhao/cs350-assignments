#!/bin/bash
source /helpers.sh

LOG=$LOGS/test_public.log
ASSIGNMENTDIR=/assignments/ASSTUSER1/

if [ ! -d /kernel ];
then
	unpack_kernel 
fi

echo "Building assignment (if no other message occurs this means it fails)"
cd /kernel
COMPILE=$LOGS/compile.log
touch $COMPILE
truncate -s 0 $COMPILE 
rm shell > /dev/null 2> /dev/null || true
gcc my_mini_shell.c -o shell >> $COMPILE 2>> $COMPILE 

touch $LOG
truncate -s 0 $LOG
touch output.txt
truncate -s 0 output.txt
touch error.txt
truncate -s 0 error.txt

# Go through basic tests
# In each of these we are essentially only testing commands with output to stdout
echo "Testing..."
for f in $ASSIGNMENTDIR/basic/*
do
	echo ">SPLIT<" >> $LOG
  bname=$(basename $f)
  echo ">TEST=$bname<"  >> $LOG
  echo ">Program Output<" >> $LOG
	timeout 1s ./shell < $f >> $LOG 2>> $LOG
  echo ">Expected Output<" >> $LOG
  bash $f >> $LOG 2>> $LOG
done

# Go through redirect and pipe tests
# In each of these we are testing stuff with redirects
# We always assume that stuff is redirected to a file called output.txt and the stderr is redirected to a file called error.txt
echo "Testing..."
for f in $ASSIGNMENTDIR/redirpipe/*
do
  echo ">SPLIT<" >> $LOG
  bname=$(basename $f)
  echo ">TEST=$bname<"  >> $LOG

  echo ">Program Output<" >> $LOG
  timeout 1s ./shell < $f >> $LOG 2>> $LOG
  echo ">Output File Contents<" >> $LOG
  cat output.txt >> $LOG
  echo ">Error File Contents<" >> $LOG
  cat error.txt >> $LOG
  truncate -s 0 output.txt
  truncate -s 0 error.txt

  echo ">Expected Output<" >> $LOG
  bash $f >> $LOG 2>> $LOG
  echo ">Output File Contents<" >> $LOG
  cat output.txt >> $LOG
  echo ">Error File Contents<" >> $LOG
  cat error.txt >> $LOG
  truncate -s 0 output.txt
  truncate -s 0 error.txt
done

exit 0 
